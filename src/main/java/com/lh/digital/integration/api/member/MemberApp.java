/**
 * The SampleMongoEventAPIApplication is Spring-Boot class to run the app.
 * Make sure you have MongoDb installed and running and configured the host and uri in the application.properties file.
 *
 * @author Vinay Domala
 * @version 1.0
 */


package com.lh.digital.integration.api.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories
@EnableAutoConfiguration
@ComponentScan
@SpringBootApplication
//@EnableCircuitBreaker
//@EnableDiscoveryClient

public class MemberApp {
	
    public static void main(String[] args) throws Exception {
        SpringApplication.run(MemberApp.class, args);
    }   
  
}


