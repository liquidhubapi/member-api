/**
* The EventController program implements RestFull API. 
* This is used to Put and Get Payload and EventId to and from MongoDB
* 
* Note : Logging and Exception handling is done by other components.
* *
* @author  Vinay Domala
* @version 1.0
*/

package com.lh.digital.integration.api.member.controller;
import com.lh.digital.integration.api.member.repository.Member;
import com.lh.digital.integration.api.member.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class MemberSearchController {

    @Autowired
    MemberRepository repository;


    @RequestMapping(value = "/member/{subscriberid}/{patientdob}/{patientfirstname}/claim",
            method = RequestMethod.GET,
            produces = APPLICATION_JSON_VALUE)
    public Member searchForClaim (
            @PathVariable("subscriberid") String subscriberid,
            @PathVariable("patientdob") String patientdob,
            @PathVariable("patientfirstname") String patientfirstname

    ) throws Exception {

        return  null ;

    }


}
