package com.lh.digital.integration.api.member.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource (path = "member", collectionResourceRel = "member")

public interface MemberRepository extends MongoRepository<Member, String>{
//    Claim findByClaim

}

